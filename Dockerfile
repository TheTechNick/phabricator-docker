FROM centos
RUN yum update -y && yum install -y httpd php php-mysql php-mbstring php-gd php-posix git sendmail
RUN sed -i 's/post_max_size = 8M/post_max_size = 64M/g' /etc/php.ini && \
sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 64M/g' /etc/php.ini && \
sed -i 's/memory_limit = 128M/memory_limit = -1/g' /etc/php.ini
WORKDIR /var/www/html
RUN git clone https://github.com/phacility/libphutil.git && \
git clone https://github.com/phacility/arcanist.git && \
git clone https://github.com/phacility/phabricator.git && \
mkdir -p /var/repo
ADD phabricator.conf /etc/httpd/conf.d/
WORKDIR /var/www/html/phabricator
CMD bin/config set mysql.host $PHABRICATOR_MYSQL_HOST && \
bin/config set mysql.user $PHABRICATOR_MYSQL_USER && \
bin/config set mysql.pass $PHABRICATOR_MYSQL_PASS && \
bin/config set phabricator.base-uri $PHABRICATOR_URI && \
bin/storage --force upgrade && \
bin/phd start && \
apachectl -DFOREGROUND
